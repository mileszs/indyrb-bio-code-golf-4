# Bio Code Golf Problem 4

The GC-content of a DNA string is given by the percentage of symbols in the string that are 'C' or 'G'. For example, the GC-content of "AGCTATAG" is 37.5%. Note that the reverse complement of any DNA string has the same GC-content.

**Given**: A DNA string (not exceeding 1 "kilobasepairs"). [ed. note - It'll be a long string, but it won't be a book.]

**Return**: The GC-content of that string rounded to 4 decimal places.

## Sample Dataset

    CCACCCTCGTGGTATGGCTAGGCATTCAGGAACCGGAGAACGCTTCAGACCAGCCCGGACTGGGAACCTGCGGGCAGTAGGTGGAAT

## Sample Output

    60.9195
